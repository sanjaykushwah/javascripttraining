window.notes = (function(db){

        var obj = {};

        obj.createNotesTable = function(){
             db.transaction(function(tx){
                tx.executeSql(
                  "CREATE TABLE notes (id INTEGER \
                   PRIMARY KEY, title TEXT, note TEXT)", [],
                  function(){ createSnackBar('Notes database created successfully!'); },
                  function(tx, error){ createSnackBar(error.message); } );
              });
        };

        obj.insertNote = function(liNote, obj){
           db.transaction(function(tx){
              tx.executeSql("INSERT INTO notes (title, note) VALUES (?, ?)", 
                             [obj.title, obj.note],
                function(tx, result){ 
                 var id = result.insertId ;
                 createSnackBar('Record ' + id+ ' saved!');
                 liNote.attr("data-id", result.insertId );
                },
                function(){ 
                  createSnackBar('The note could not be saved.'); 
                }
              );
           });
        };


        obj.fetchNotes = function(){
          db.transaction(function(tx) {
              tx.executeSql('SELECT id, title, note FROM notes', [],
                function(SQLTransaction, data){
                  for (var i = 0; i < data.rows.length; ++i) {
                      var row = data.rows.item(i);
                      addToNotesList(row);
                  }
              });
          });
        };

        obj.updateNote = function(id, obj){
           db.transaction(function(tx){
            tx.executeSql("UPDATE notes set title = ?, note = ? where id = ?",
                          [obj.title, obj.note, id],
              function(tx, result){ 
                createSnackBar('Record ' + id + ' updated!');
                $("#notes>li[data-id=" + id + "]").html(obj.title);
              },
              function(){ 
                createSnackBar('The note was not updated!');
              }
            );
          });
        };

        obj.deleteNote = function(id) {
           db.transaction(function(tx){
              tx.executeSql("DELETE from notes where id = ?", [id],
                function(tx, result){ 
                 createSnackBar('Record ' + id + ' deleted!');
                 $("#notes ul>li[data-id=" + id + "]").remove();
                },
                function(){ 
                 createSnackBar('The note was not deleted!');
                }
              );
           });
        };


        obj.loadNote = function(id){
          db.transaction(function(tx) {
            tx.executeSql('SELECT id, title, note FROM notes where id = ?', [id],
              function(SQLTransaction, data){
                var row = data.rows.item(0);
                var title = $("#title");
                var note = $("#note");

                title.val(row["title"]);
                title.attr("data-id", row["id"]);
                note.val(row["note"]);
                $("#delete").show();

              });
          });
        }



        return obj;
})(db);