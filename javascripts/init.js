window.app = {
  "version" : "0.1",
   "author" : "Sanjay Kushwah",
};

window.db = window.openDatabase('noteoffline', '1.0', 'Notes Database', 1024*1024*3);

function createSnackBar(messege){
    $("#snackbar").addClass("show").html(messege);
    setTimeout(function(){ $("#snackbar").removeClass("show"); }, 3000);
}