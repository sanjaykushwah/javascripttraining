

addToNotesList = function(obj){
  var notes = $("#notes ul");
  var item = $("<li>");
  item.attr("data-id", obj.id);
  var a = $("<a>");
  a.html(`<span class="delete">&times;</span>`)
  a.append(`<div class='title'>${obj.title}</div>`);
  a.append(`<div class='note'>${obj.note}</div>`);
  item.append(a);
  notes.append(item);
};


// Fetching notes



newNote = function(){
  $("#delete").hide();
  var title = $("#title");
  title.removeAttr("data-id");
  title.val("");
  var note = $("#note");
  note.val("");
}


$(document).off("click", "li .title, li .note").on("click", "li .title, li .note", function(){
   $(this).prop('contenteditable', true).addClass("editable");
});

$(document).off("click", "li .delete").on("click", "li .delete", function(){
   var id = $(this).closest("li").data("id");
   if(confirm("Do you really want to delete this note")){
       notes.deleteNote(id);
   }
});


$(document).off("focusout", "li .title, li .note").on("focusout", "li .title, li .note", function(){
  var parent = $(this).closest("li");
   $(this).prop('contenteditable', false).removeClass("editable");
    notes.updateNote(parent.data("id"), {
       title: parent.find(".title").text(),
       note: parent.find(".note").text(),
    });
});


$(function(){
  

  $("#form").hide();
  $("#delete").hide();
  
  
  $("#new").click(function(e){
    $("#form").show();
    $("#notes").hide();
    newNote();
  });
  
  $("#back").click(function(e){
    $("#form").hide();
    $("#notes").show();
    
  });
  
  notes.createNotesTable();
  notes.fetchNotes();
  
  
  $("#save").click(function(event){
    event.preventDefault();
    var title = $("#title");
    var note = $("#note");
    // Check to see if it's an existing entry
    if(title.attr("data-id")){
      notes.updateNote(title, note);
    }else{
      notes.insertNote(title, note);
    }
  });
    
  $("#notes").click(function(event){
    if ($(event.target).is('li')) {
      var element = $(event.target);
      notes.loadNote(element.attr("data-id"));
      $("#form").show();
      $("#notes").hide();
      
    }
    
  });
  
  $("#delete").click(function(event){
    event.preventDefault();
    var title = $("#title");
    notes.deleteNote(title);
    newNote();
    $("#back").click();
  });
  
  

});
